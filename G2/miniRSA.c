#include "miniRSA.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

int coprime(int x)
{
    int r = rand();
    srand(time(NULL));

    while (GCD(x, r) != 1 && r != 1)
        r = rand();
        
    return r;
}

int endecrypt(int msg_or_cipher, int key, int c)
{
    return modulo(msg_or_cipher, key, c);
}

// Euclidean algorithm
int GCD(int a, int b)
{
    if (a % b == 0)
        return b;
    else 
        return GCD(b, a % b);
}

// Iterative extended Euclidean algorithm
int mod_inverse(int base, int m)
{
    int origModulo = m;
    int y = 0, x = 1;
 
    if (m == 1)
        return 0;
 
    while (base > 1)
    {
        // Integer division to get quotient without remainder
        int quotient = base / m;
        int t = m; // Store m so it's not lost
 
        // Euclidean algorithm
        m = base % m;
        base = t;
        t = y;
 
        // Keep track of our extended Euclid integers
        y = x - quotient * y;
        x = t;
    }
 
    // Make x positive
    if (x < 0)
       x += origModulo;
 
    return x;
}

int modulo(int a, int b, int c)
{
    return (int)pow(a, b) % c;
}   

int totient(int n)
{
    int result = n;

    for (int p = 2; p * p <= n; ++p)
    {
        if (n % p == 0)
        {
            while (n % p == 0)
                n /= p;
            result *= (1.0 - (1.0 / (float)p));
        }
    }

    if (n > 1)
        result *= (1.0 - (1.0 / (float)n));

    return result;
}
#ifndef MINIRSA_H_
#define MINIRSA_H_

int coprime(int x);

int endecrypt(int msg_or_cipher, int key, int c);

int GCD(int a, int b);

int mod_inverse(int base, int m);

int modulo(int a, int b, int c);

int totient(int n);

#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include "miniRSA.h"

#define PORT 12597
#define MAXSTRING 1000

void keygen(int a, int b, int *e, int *d, int *c)
{
    *c = a * b;
    int m = (a - 1) * (b - 1);

    *e = m * 2;
    while (GCD(*e, m) != 1 || GCD(*e, *c) != 1 || *e % m <= 1)
    {
        (*e)++;
    }

    *d = mod_inverse(*e, m);
}

int main()
{
    int a = 97, b = 173;
    int e, d, c;

    keygen(a, b, &e, &d, &c);

    printf("e = %d, d = %d\n", e, d);

    char buff[MAXSTRING + 1];
    int mySocket, length;
    struct sockaddr_in server;
    struct hostent *hp;

    mySocket = socket(AF_INET, SOCK_STREAM, 0);
    if (mySocket == -1)
    {
        perror("Socket error");
        return 1;
    }

    if ((hp = gethostbyname("tux5.cs.drexel.edu")) == NULL)
    {
        perror("Host error");
        return -2;
    }

    bzero((char *) &server, sizeof(server)); 
    //server.sin_addr.s_addr = htonl(INADDR_LOOPBACK); //localhost
    server.sin_family = AF_INET;
    bcopy((char *)hp->h_addr_list[0],
          (char *)&server.sin_addr.s_addr, hp->h_length);

    server.sin_port = htons(PORT);

    if (connect(mySocket, (struct sockaddr *)&server, sizeof(struct sockaddr_in)) < 0)
    {
        perror("Connection error");
        return -1;
    }

    length = recv(mySocket, buff, MAXSTRING, 0);
    buff[length] = '\0';

    printf("Received %s (%d bytes).\n", buff, length);

    close(mySocket);

    return 0;
}

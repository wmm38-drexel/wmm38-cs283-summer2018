#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include "miniRSA.h"

#define PORT 12597

int main()
{
    char *msg = "Hello World !\n";

    struct sockaddr_in client;
    struct sockaddr_in server;
    int mySocket;
    socklen_t socksize = sizeof(struct sockaddr_in);

    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(PORT);

    mySocket = socket(AF_INET, SOCK_STREAM, 0);
    if (mySocket == -1)
    {
        perror("Socket error");
        return 1;
    }

    if (bind(mySocket, (struct sockaddr *)&server, sizeof(struct sockaddr)) != 0)
    {
        perror("Bind error");
        return 1;
    }

    listen(mySocket, 1);
    int consocket = accept(mySocket, (struct sockaddr *)&client, &socksize);

    while (consocket)
    {
        printf("Incoming connection from %s - sending welcome\n", inet_ntoa(client.sin_addr));
        send(consocket, msg, strlen(msg), 0);
        close(consocket);
        consocket = accept(mySocket, (struct sockaddr *)&client, &socksize);
    }

    close(mySocket);

    return 0;
}

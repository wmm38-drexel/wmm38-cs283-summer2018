#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAXLINES 1000
#define MAXINPUTSIZE 100
#define MAXSTRINGSIZE 20
#define MAXARRAYSIZE 10

//Trims brackets and commas off of field names (moves forward pointer past prepended chars and replaces appended chars with null terminator)
char* trimFields(char* str)
{
    char* end;

    if (*str == '[' || *str == '(')
        str++;
    
    end = str + strlen(str) - sizeof(char);

    while (*end == ']' || *end == ')' || *end == ',')
        end--;
    *(end + sizeof(char)) = '\0';

    return str;
}

char** getLines(char* tableName)
{
    char** lines = malloc(sizeof(char*) * MAXLINES);
    char line[MAXINPUTSIZE];
    int i = 0;

    //Open file
    FILE* table;
    table = fopen(tableName, "r");
    
    if (table != NULL)
    {
        while (fgets(line, sizeof(line), table))
        {
            lines[i] = malloc(sizeof(char) * MAXINPUTSIZE);
            strcpy(lines[i], line);
            i++;
        }
    }
    else
    {
        printf("%s", "Table does not exist.");
    }

    lines[i] = '\0';
	fclose(table);
    return lines;
}

int main()
{
    //Get input
    char* input = malloc(sizeof(char) * MAXINPUTSIZE);
    fgets(input, MAXINPUTSIZE, stdin);

    //Strip newline
    char *pos;
    if ((pos=strchr(input, '\n')) != NULL)
        *pos = '\0';

    //Tokenize input
    char** tokenArray = malloc(sizeof(char*) * MAXARRAYSIZE);
    char* token = strtok(input, " ");

    int i = 0;
    while (token != NULL)
    {
        tokenArray[i] = token;
        token = strtok(NULL, " ");
        i++;
    }
    tokenArray[i] = NULL;

    free(token);

    //Parse tokens. This is a monster of an if-else tree so I've got comments on each if-else to let you know where you're at.
    if (strcmp(tokenArray[0], "CREATE") == 0) //If first command is create
    {
        if (tokenArray[4] == NULL) //If there aren't enough inputs for a CREATE command
        {
            fputs("Not enough inputs for this command.", stdout);
        }
        else //If there are enough inputs
        {
            if (strcmp(tokenArray[1], "TABLE") == 0) //If command so far is CREATE TABLE
            {
                //Assume this is the table name for now
                char* tableName = malloc(sizeof(char) * MAXSTRINGSIZE);
                strcpy(tableName, tokenArray[2]);

                if (strcmp(tokenArray[3], "FIELDS") == 0) //If command so far is CREATE TABLE {TableName} FIELDS
                {
                    char** fields = malloc(sizeof(char*) * MAXARRAYSIZE);

                    //Get first field into array
                    tokenArray[4] = trimFields(tokenArray[4]);
                    fields[0] = malloc(sizeof(char) * (strlen(tokenArray[4]) + 1));
                    strcpy(fields[0], tokenArray[4]);
                    
                    //Loop through rest of fields storing them as well
                    int fieldIndex = 1;
                    int arrayIndex = 5;
                    while (tokenArray[arrayIndex] != NULL)
                    {
                        tokenArray[arrayIndex] = trimFields(tokenArray[arrayIndex]);
                        fields[fieldIndex] = malloc(sizeof(char) * (strlen(tokenArray[arrayIndex]) + 1));
                        strcpy(fields[fieldIndex], tokenArray[arrayIndex]);
                        fieldIndex++;
                        arrayIndex++;
                    }
                    fields[fieldIndex] = NULL;

                    //Create comma separated line out of field names
                    char* header = malloc(sizeof(char) * MAXINPUTSIZE);
                    fieldIndex = 0;
                    int length = 0;
                    while(fields[fieldIndex] != NULL)
                    {
                        //Append element of fields to header
                        memcpy(header + (length * sizeof(char)), fields[fieldIndex], strlen(fields[fieldIndex]));

                        //Keep track of how many chars header has
                        length += (strlen(fields[fieldIndex]) + sizeof(char));

                        //Add comma
                        *(header + (length * sizeof(char)) - sizeof(char)) = ',';
                        fieldIndex++;
                    }
                    //Replace last comma with new line and add null terminator
                    *(header + (length * sizeof(char)) - sizeof(char)) = '\n';
                    *(header + (length * sizeof(char))) = '\0';

                    //Create file and add data
                    FILE* table;
                    table = fopen(tableName, "w");
                    fputs(header, table);
                    fclose(table);

                    //Free memory
                    int j = 0;
                    while (fields[j] != NULL)
                    {
                        free(fields[j]);
                        j++;
                    }
                    free(fields);
					free(header);
                }
                else //If command was CREATE TABLE {TableName} but then not FIELDS
                {
                    fputs("Please check your input.", stdout);
                }

                free(tableName);
            }
            else //If command was CREATE but then not TABLE
            {
                fputs("Please check your input.", stdout);
            }
        }
    }
    else if (strcmp(tokenArray[0], "INSERT") == 0) //If first command is insert
    {
        if (tokenArray[3] == NULL) //If there aren't enough inputs for an insert command
        {
            fputs("Not enough inputs for this command.", stdout);
        }
        else
        {
            if (strcmp(tokenArray[1], "INTO") == 0) //If command so far is INSERT INTO
            {
                //Get table name
                char* tableName = malloc(sizeof(char) * MAXSTRINGSIZE);
                strcpy(tableName, tokenArray[2]);

                char** fields = malloc(sizeof(char*) * MAXARRAYSIZE);

                //Get first field into array
                tokenArray[3] = trimFields(tokenArray[3]);
                fields[0] = malloc(sizeof(char) * (strlen(tokenArray[3]) + 1));
                strcpy(fields[0], tokenArray[3]);
                    
                //Loop through rest of fields storing them as well
                int fieldIndex = 1;
                int arrayIndex = 4;
                while (tokenArray[arrayIndex] != NULL)
                {
                    tokenArray[arrayIndex] = trimFields(tokenArray[arrayIndex]);
                    fields[fieldIndex] = malloc(sizeof(char) * (strlen(tokenArray[arrayIndex]) + 1));
                    strcpy(fields[fieldIndex], tokenArray[arrayIndex]);
                    fieldIndex++;
                    arrayIndex++;
                }
                fields[fieldIndex] = NULL;

                //Remove all but stuff between quotes
                fieldIndex = 0;
                while (fields[fieldIndex] != NULL)
                {
                    char* end = fields[fieldIndex] + strlen(fields[fieldIndex]) - sizeof(char);
                    *end = '\0';
                    memmove(fields[fieldIndex], strchr(fields[fieldIndex], '\"') + sizeof(char), end - strchr(fields[fieldIndex], '\"'));
                    fieldIndex++;
                }

                //Create row from fields
                char* rowToAdd = malloc(sizeof(char) * MAXINPUTSIZE);
                fieldIndex = 0;
                int length = 0;
                while(fields[fieldIndex] != NULL)
                {
                    //Append element of fields to rowToAdd
                    memcpy(rowToAdd + (length * sizeof(char)), fields[fieldIndex], strlen(fields[fieldIndex]));
                    //Keep track of how many chars rowToAdd has
                    length += (strlen(fields[fieldIndex]) + sizeof(char));
                    //Add comma
                    *(rowToAdd + (length * sizeof(char)) - sizeof(char)) = ',';
                    fieldIndex++;
                }
                //Replace last comma with new line and add null terminator
                *(rowToAdd + (length * sizeof(char)) - sizeof(char)) = '\n';
                *(rowToAdd + (length * sizeof(char))) = '\0';

                //Create file and add data
                FILE* table;
                table = fopen(tableName, "a");
                fputs(rowToAdd, table);
                fclose(table);

                //Free memory
                fieldIndex = 0;
                while (fields[fieldIndex] != NULL)
                {
                    free(fields[fieldIndex]);
                    fieldIndex++;
                }
                free(rowToAdd);
                free(fields);
                free(tableName);
            }
            else //If command was INSERT but then not INTO
            {
                fputs("Please check your input.", stdout);
            }
        }
    }
    else if (strcmp(tokenArray[0], "SELECT") == 0) //If first command is select
    {
        if (tokenArray[5] == NULL)
        {
            fputs("Not enough inputs for this command.", stdout);
        }
        else
        {
            if (strcmp(tokenArray[1], "*") == 0) //If command so far is SELECT *
            {
                if (strcmp(tokenArray[2], "FROM") == 0) //If command so far is SELECT * FROM
                {
                    //Assume this is the table name for now
                    char* tableName = malloc(sizeof(char) * MAXSTRINGSIZE);
                    strcpy(tableName, tokenArray[3]);

                    if(strcmp(tokenArray[4], "WHERE") == 0) //If command so far is SELECT * FROM {TableName} WHERE
                    {
                        //Read in the file
                        char** lines = getLines(tableName);

                        //Isolate field name and value to match
                        char* fieldName = malloc(sizeof(char) * MAXSTRINGSIZE);
                        char* value = calloc(MAXSTRINGSIZE, sizeof(char) * MAXSTRINGSIZE);
                        strcpy(fieldName, tokenArray[5]);

                        char* end = fieldName + strlen(fieldName) - sizeof(char);
                        memcpy(value, strchr(fieldName, '\"') + sizeof(char), end - strchr(fieldName, '\"'));
                        *(value + strlen(value) * sizeof(char) - sizeof(char)) = '\0';
                        *strchr(fieldName, '=') = '\0';
                        
                        //Find which column the field we're looking for is in
                        int colNum = 0;
                        char* fieldIndex = strstr(lines[0], fieldName);
                        char* currentIndex = lines[0];
                        while (*currentIndex != '\0' && currentIndex < fieldIndex)
                        {
                            if (*currentIndex == ',')
                                colNum++;
                            currentIndex += sizeof(char);
                        }

                        //Loop through lines and print out columns with matches
                        int linesIndex = 1;
                        while (lines[linesIndex] != NULL)
                        {
                            char* line = malloc(sizeof(char*) * MAXINPUTSIZE);
                            strcpy(line, lines[linesIndex]);

                            //Strip newline
                            char *pos;
                            if ((pos=strchr(line, '\n')) != NULL)
                                *pos = '\0';

                            //Tokenize input
                            char** lineArray = malloc(sizeof(char*) * MAXARRAYSIZE);
                            char* tok = strtok(line, ",");

                            int i = 0;
                            while (tok != NULL)
                            {
                                lineArray[i] = malloc(sizeof(char) * MAXINPUTSIZE);
                                strcpy(lineArray[i], tok);
                                tok = strtok(NULL, ",");
                                i++;
                            }
                            lineArray[i] = NULL;

                            //Print if value matches
                            if(strcmp(lineArray[colNum], value) == 0)
                            {
                                fputs(lines[linesIndex], stdout);
                            }
                            linesIndex++;

                            //Free memory
                            i = 0;
                            while (lineArray[i] != NULL)
                            {
                                free(lineArray[i]);
                                i++;
                            }
                            free(lineArray);
                            free(line);
                        }

                        //Free memory
                        linesIndex = 0;
                        while (lines[linesIndex] != NULL)
                        {
                            free(lines[linesIndex]);
                            linesIndex++;
                        }
                        free(lines);
                        free(fieldName);
                        free(value);
                    }
                    else //If command was SELECT * FROM but then not WHERE
                    {
                        fputs("Please check your input.", stdout);
                    }

                    free(tableName);
                }
                else //If command was SELECT * but then not FROM
                {
                    fputs("Please check your input.", stdout);
                }
            }
            else //If command was SELECT but then not *
            {
                fputs("Please check your input.", stdout);
            }
        }
    }
    else if (strcmp(tokenArray[0], "DROP") == 0) //If first command is drop
    {
        if (tokenArray[2] == NULL)
        {
            fputs("Not enough inputs for this command.", stdout);
        }
        else
        {
            if (strcmp(tokenArray[1], "TABLE") == 0) //if command so far is DROP TABLE
            {
                char* tableName = malloc(sizeof(char) * MAXSTRINGSIZE);
                strcpy(tableName, tokenArray[2]);

                if (remove(tableName) == 0)
                    printf("%s%s\n", "Dropped table: ", tableName);
                else
                    printf("%s%s\n", "Unable to drop table: ", tableName);

                free(tableName);
            }
            else //If command was DROP but then not TABLE
            {
                fputs("Please check your input.", stdout);
            }
        }
    }
    else if (strcmp(tokenArray[0], "DELETE") == 0) //If first command is delete
    {
        if (tokenArray[4] == NULL)
        {
            fputs("Not enough inputs for this command.", stdout);
        }
        else
        {
            if (strcmp(tokenArray[1], "FROM") == 0) //If command so far is DELETE FROM
                {
                    //Assume this is the table name for now
                    char* tableName = malloc(sizeof(char) * MAXSTRINGSIZE);
                    strcpy(tableName, tokenArray[2]);

                    if(strcmp(tokenArray[3], "WHERE") == 0) //If command so far is DELETE FROM {TableName} WHERE
                    {
                        //Read in the file
                        char** lines = getLines(tableName);

                        //Isolate field name and value to match
                        char* fieldName = malloc(sizeof(char) * MAXSTRINGSIZE);
                        char* value = calloc(MAXSTRINGSIZE, sizeof(char) * MAXSTRINGSIZE);
                        strcpy(fieldName, tokenArray[4]);

                        char* end = fieldName + strlen(fieldName) - sizeof(char);
                        memcpy(value, strchr(fieldName, '\"') + sizeof(char), end - strchr(fieldName, '\"'));
                        *(value + strlen(value) * sizeof(char) - sizeof(char)) = '\0';
                        *strchr(fieldName, '=') = '\0';

                        //Find which column the field we're looking for is in
                        int colNum = 0;
                        char* fieldIndex = strstr(lines[0], fieldName);
                        char* currentIndex = lines[0];
                        while (*currentIndex != '\0' && currentIndex < fieldIndex)
                        {
                            if (*currentIndex == ',')
                                colNum++;
                            currentIndex += sizeof(char);
                        }

                        
                        //Open new file for writing
                        FILE* temp;
                        temp = fopen("tempTable", "w");
                        fputs(lines[0], temp);

                        //Loop through lines and write lines that don't match to new file
                        int linesIndex = 1;
                        while (lines[linesIndex] != NULL)
                        {
                            char* line = malloc(sizeof(char*) * MAXINPUTSIZE);
                            strcpy(line, lines[linesIndex]);

                            //Strip newline
                            char *pos;
                            if ((pos=strchr(line, '\n')) != NULL)
                                *pos = '\0';

                            //Tokenize input
                            char** lineArray = malloc(sizeof(char*) * MAXARRAYSIZE);
                            char* tok = strtok(line, ",");

                            int i = 0;
                            while (tok != NULL)
                            {
                                lineArray[i] = malloc(sizeof(char) * MAXINPUTSIZE);
                                strcpy(lineArray[i], tok);
                                tok = strtok(NULL, ",");
                                i++;
                            }
                            lineArray[i] = NULL;

                            //Print if value doesn't match
                            if(strcmp(lineArray[colNum], value) != 0)
                            {
                                fputs(lines[linesIndex], temp);
                            }
                            linesIndex++;

                            //Free memory
                            i = 0;
                            while (lineArray[i] != NULL)
                            {
                                free(lineArray[i]);
                                i++;
                            }
                            free(lineArray);
                            free(line);
                        }

                        //Rename table to replace old one
                        rename("tempTable", tableName);

                        //Free memory
                        linesIndex = 0;
                        while (lines[linesIndex] != NULL)
                        {
                            free(lines[linesIndex]);
                            linesIndex++;
                        }
                        free(lines);
                        free(fieldName);
                        free(value);
                        fclose(temp);
                    }
                    else //If command was DELETE FROM {TableName} but then not WHERE
                    {
                        fputs("Please check your input.", stdout);
                    }

                    free(tableName);
                }
        }
    }
    else if (strcmp(tokenArray[0], "UPDATE") == 0) //If first command is update
    {
        if (tokenArray[5] == NULL)
        {
            fputs("Not enough inputs for this command.", stdout);
        }
        else
        {
            //Assume this is the table name for now
            char* tableName = malloc(sizeof(char) * MAXSTRINGSIZE);
            strcpy(tableName, tokenArray[1]);

            if (strcmp(tokenArray[2], "SET") == 0) //If command so far is UPDATE {TableName} SET
            {
                //Isolate field name and value to match
                char* fieldNameToAdd = malloc(sizeof(char) * MAXSTRINGSIZE);
                char* valueToAdd = calloc(MAXSTRINGSIZE, sizeof(char) * MAXSTRINGSIZE);
                strcpy(fieldNameToAdd, tokenArray[3]);

                char* endToAdd = fieldNameToAdd + strlen(fieldNameToAdd) - sizeof(char);
                memcpy(valueToAdd, strchr(fieldNameToAdd, '\"') + sizeof(char), endToAdd - strchr(fieldNameToAdd, '\"'));
                *(valueToAdd + strlen(valueToAdd) * sizeof(char) - sizeof(char)) = '\0';
                *strchr(fieldNameToAdd, '=') = '\0';

                if (strcmp(tokenArray[4], "WHERE") == 0) //If command so far is UPDATE {TableName} SET WHERE
                {
                    //Read in the file
                    char** lines = getLines(tableName);


                    //Isolate field name and value to match
                    char* fieldName = malloc(sizeof(char) * MAXSTRINGSIZE);
                    char* value = calloc(MAXSTRINGSIZE, sizeof(char) * MAXSTRINGSIZE);
                    strcpy(fieldName, tokenArray[5]);

                    char* end = fieldName + strlen(fieldName) - sizeof(char);
                    memcpy(value, strchr(fieldName, '\"') + sizeof(char), end - strchr(fieldName, '\"'));
                    *(value + strlen(value) * sizeof(char) - sizeof(char)) = '\0';
                    *strchr(fieldName, '=') = '\0';

                    //Find which column the field we're looking for is in
                    int colNumToAdd = 0;
                    char* fieldIndex = strstr(lines[0], fieldNameToAdd);
                    char* currentIndex = lines[0];
                    while (*currentIndex != '\0' && currentIndex < fieldIndex)
                    {
                        if (*currentIndex == ',')
                            colNumToAdd++;
                        currentIndex += sizeof(char);
                    }

                    //Find which column the field we're looking for is in
                    int colNum = 0;
                    fieldIndex = strstr(lines[0], fieldName);
                    currentIndex = lines[0];
                    while (*currentIndex != '\0' && currentIndex < fieldIndex)
                    {
                        if (*currentIndex == ',')
                            colNum++;
                        currentIndex += sizeof(char);
                    }

                    //Open new file for writing
                    FILE* temp;
                    temp = fopen("tempTable", "w");
                    fputs(lines[0], temp);

                    int linesIndex = 1;
                    while (lines[linesIndex] != NULL)
                    {
                        char* line = malloc(sizeof(char*) * MAXINPUTSIZE);
                        strcpy(line, lines[linesIndex]);

                        //Strip newline
                        char *pos;
                        if ((pos=strchr(line, '\n')) != NULL)
                                *pos = '\0';

                        //Tokenize input
                        char** lineArray = malloc(sizeof(char*) * MAXARRAYSIZE);
                        char* tok = strtok(line, ",");

                        int i = 0;
                        while (tok != NULL)
                        {
                            lineArray[i] = malloc(sizeof(char) * MAXINPUTSIZE);
                            strcpy(lineArray[i], tok);
                            tok = strtok(NULL, ",");
                            i++;
                        }
                        lineArray[i] = NULL;

                        if (strcmp(lineArray[colNum], value) == 0) //If this line has a value that needs to change then loop through each token and put them in manually
                        {
                            int lineArrayIndex = 1;
                            while (lineArray[lineArrayIndex] != NULL)
                            {
                                if (lineArrayIndex - 1 == colNumToAdd)
                                {
                                    fputs(valueToAdd, temp);
                                }
                                else
                                {
                                    fputs(lineArray[lineArrayIndex - 1], temp);
                                }
                                fputs(",", temp);
                                lineArrayIndex++;
                            }
                            if (lineArrayIndex - 1 == colNumToAdd)
                                {
                                    fputs(valueToAdd, temp);
                                    fputs("\n", temp);
                                }
                                else
                                {
                                    fputs(lineArray[lineArrayIndex - 1], temp);
                                }
                        }
                        else
                        {
                            fputs(lines[linesIndex], temp);
                        }

                        //Rename table to replace old one
                        rename("tempTable", tableName);

                        //Free memory
                        i = 0;
                        while (lineArray[i] != NULL)
                        {
                            free(lineArray[i]);
                            i++;
                        }
                        free(lineArray);
                        free(line);

                        linesIndex++;
                    }

                    //Free memory
                    linesIndex = 0;
                    while (lines[linesIndex] != NULL)
                    {
                        free(lines[linesIndex]);
                        linesIndex++;
                    }
						  free(lines);
                    free(fieldName);
                    free(value);
                    fclose(temp);
                }
                else //If command was UPDATE {TableName} SET but then not WHERE
                {
                    fputs("Please check your input.", stdout);
                }

                free(fieldNameToAdd);
                free(valueToAdd);
            }
            else //If command was UPDATE {TableName} but then not SET
            {
                fputs("Please check your input.", stdout);
            }

            free(tableName);
        }
    }

    free(input);
	free(tokenArray);

    return 0;
}

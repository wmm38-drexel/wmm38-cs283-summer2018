#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

struct SharedData
{
	int *count;
	pthread_mutex_t *mutex;
	int id;
};

void *do_work(void *arg)
{
	struct SharedData *data;
	pthread_mutex_t *mutex;

	data = (struct SharedData *)arg;
	mutex = data->mutex;
	
	//pthread_mutex_lock(mutex);

	for (int j = 0; j < 1000; j++)
	{
		pthread_mutex_lock( mutex );
		int* count = data->count;
		(*count)++;
		pthread_mutex_unlock( mutex );
	}

	printf("Thread number %d is done.\n", data->id);

	//pthread_mutex_unlock(mutex);

	free(data);
}

int main()
{
	int count = 0;
	pthread_t *arr = malloc(sizeof(pthread_t) * 100);

	pthread_mutex_t lock;
	pthread_mutex_init(&lock, NULL);

	for (int i = 0; i < 100; i++)
	{
		struct SharedData *x = malloc(sizeof(struct SharedData));
		x->id = i;
		x->mutex = &lock;
		x->count = &count;
		pthread_t t;
		arr[i] = t;
		pthread_create(&t, NULL, do_work, (void *)x);
	}

	for (int i = 0; i < 100; i++)
	{
		pthread_join(arr[i], NULL);
	}
	printf("Count: %d\n", count);

	free(arr);

	return 0;
}

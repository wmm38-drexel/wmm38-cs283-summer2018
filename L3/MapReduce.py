import threading
import sys

class Airport:
    def __init__(self, name):
        self.name = name
        self.totalSecWeathDelay = 0
        self.totalLateAircraftDelay = 0
        self.numSecWeathDelays = 0
        self.numLateAircraftDelays = 0

    def addSecWeathDelay(self, a):
        self.totalSecWeathDelay += a.totalSecWeathDelay
        self.numSecWeathDelays += 1

    def addLateAircraftDelay(self, a):
        self.totalLateAircraftDelay += a.totalLateAircraftDelay
        self.numLateAircraftDelays += 1

    def averageSecWeathDelay(self):
        if self.numSecWeathDelays != 0:
            return self.totalSecWeathDelay / self.numSecWeathDelays
        else:
            return 0
        
    def averageLateAircraftDelay(self):
        if self.numLateAircraftDelays != 0:
            return self.totalLateAircraftDelay / self.numLateAircraftDelays
        else:
            return 0

    def __hash__(self):
        return hash(self.name)
    
    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return not(self.name == other.name)

class Carrier:
    def __init__(self, name):
        self.name = name
        self.totalCarrierDelay = 0
        self.totalLateAircraftDelay = 0
        self.numCarrierDelays = 0
        self.numLateAircraftDelays = 0
    
    def addCarrierDelay(self, c):
        self.totalCarrierDelay += c.totalCarrierDelay
        self.numCarrierDelays += 1

    def addLateAircraftDelay(self, a):
        self.totalLateAircraftDelay += a.totalLateAircraftDelay
        self.numLateAircraftDelays += 1
    
    def averageCarrierDelay(self):
        if self.numCarrierDelays != 0:
            return self.totalCarrierDelay / self.numCarrierDelays
        else:
            return 0
        
    def averageLateAircraftDelay(self):
        if self.numLateAircraftDelays != 0:
            return self.totalLateAircraftDelay / self.numLateAircraftDelays
        else:
            return 0

    def __hash__(self):
        return hash(self.name)
    
    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return not(self.name == other.name)

airports = {} # Dict for airports
carriers = {} # Dict for carriers
airportLock = threading.Lock() # Lock for airport dict
carrierLock = threading.Lock() # Lock for carrier dict
lines = [] # Array for lines

# Get the number of threads to use from user
print("Please enter the number of nodes to use:")
numNodes = int(sys.stdin.readline())

# Open file and read it into an array
with open("DelayedFlights.csv") as f:
    lines = f.readlines()
# Pop header row
lines.pop(0)

# Loop through the lines
def readinto(begin, end):
    for x in range(int(begin), int(end)):
        line = lines[x].split(",")

        # Weather + Security delay
        if line[26] != "" and line[28] != "":
            a = Airport(line[17])
            a.totalSecWeathDelay = float(line[26]) + float(line[28])
            airportLock.acquire()
            if not (a.name in airports):
                airports[line[17]] = a
            else:
                airports[line[17]].addSecWeathDelay(a)
            airportLock.release()

        # Late aircraft delay (airport)
        if line[29] != "\n":
            a = Airport(line[17])
            a.totalLateAircraftDelay = float(line[29])
            airportLock.acquire()
            if not (a.name in airports):
                airports[line[17]] = a
            else:
                airports[line[17]].addLateAircraftDelay(a)
            airportLock.release()

        # Carrier delay
        if line[25] != "":
            c = Carrier(line[9])
            c.totalCarrierDelay = float(line[25])
            carrierLock.acquire()
            if not c.name in carriers:
                carriers[line[9]] = c
            else:
                carriers[line[9]].addCarrierDelay(c)
            carrierLock.release()

        # Late aircraft delay (carrier)
        if line[29] != "\n":
            c = Carrier(line[9])
            c.totalLateAircraftDelay = float(line[29])
            carrierLock.acquire()
            if not (c.name in carriers):
                carriers[line[9]] = c
            else:
                carriers[line[9]].addLateAircraftDelay(c)
            carrierLock.release()

threads = [] # List for threads

# Create threads and give them a chunk of the lines and tell them to put them in dicts
for i in range(numNodes):
    start = int(i * len(lines) / numNodes)
    end = int((i + 1) * len(lines) / numNodes - 1)
    t = threading.Thread(target=readinto, args=(start, end))
    threads.append(t)
    t.start()

# Make sure threads are all done before sorting and printing
for t in threads:
    if not t.name == 'MainThread':
        t.join()
    
# Sort airports by average security + weather delays and print
sortedSecWeath = sorted(airports.values(), key=lambda x: x.averageSecWeathDelay(), reverse=True)

print( "----- AIRPORT DELAYS DUE TO WEATHER + SECURITY -----" )
for a in sortedSecWeath:
    print(a.name + ": " + str(a.averageSecWeathDelay()))
print( "----------------------------------\n\n" )

# Sort airports by average late aircraft delays and print
sortedLateAircraft = sorted(airports.values(), key=lambda x: x.averageLateAircraftDelay(), reverse=True)

print( "----- AIRPORT DELAYS DUE TO LATE AIRCRAFTS -----" )
for a in sortedLateAircraft:
    print(a.name + ": " + str(a.averageLateAircraftDelay()))
print( "----------------------------------\n\n" )

# Sort carriers by average carrier delay
sortedCarrierDelay = sorted(carriers.values(), key=lambda x: x.averageCarrierDelay(), reverse=True)

print ( "----- CARRIER DELAYS -----" )
for c in sortedCarrierDelay:
    print(c.name + ": " + str(c.averageCarrierDelay()))
print( "----------------------------------\n\n" )

# Sort carriers by average late aircraft delays and print

sortedCarrierLateAircraft = sorted(carriers.values(), key=lambda x: x.averageLateAircraftDelay(), reverse=True)

print( "----- CARRIER DELAYS DUE TO LATE AIRCRAFTS -----" )
for c in sortedCarrierLateAircraft:
    print(c.name + ": " + str(c.averageLateAircraftDelay()))
print( "----------------------------------\n\n" )
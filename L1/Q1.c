#include <stdlib.h>
#include <stdio.h>

int main() {
	int* ptr = malloc(sizeof(int) * 10);

	for(int i = 0; i < 10; i++)
	{
		ptr[i] = i;
	}

	for (int i = 0; i < 10; i++)
	{
		printf("%d\n", ptr[i]);
	}

	free(ptr);
}

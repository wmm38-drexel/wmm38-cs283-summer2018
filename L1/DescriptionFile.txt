Wills Martin
Systems Programming
Lab1

I split my lab up into five different files, one for each question. Question
one creates a array of integers, prints them, and frees them, all using
pointers. Question two creates an array of strings, each 10 chars in length,
and prints them. Question three sorts an integer array using pointer
arithmetic. Question four sorts a linked list of nodes by swapping the
nodes. Question five creates an array of integers that can be added to
without having to worry about the initial allocation size; if you try to
allocate beyond it's initial size it will resize for you automatically.

I did all of my work on Tux, so I used the technology available on tux: vi
version 7.4.1689, gcc version 5.4.0, and valgrind. I also used
pythontutor.com

No settings or parameters. Just run the makefile and run the output files
(will be under the pattern Q?.out).

I tested using valgrind and pythontutor.com. All programs were successful.

A little more cohesion between the questions might help some people
understand the concepts better. For example, have the questions lead up to a
final question where you use the code you wrote before to make a short
program that actually does something that isn't just abstract.

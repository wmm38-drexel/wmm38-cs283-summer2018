#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
	char** ptr = malloc(sizeof(char*) * 10);

	for(int i = 0; i < 10; i++)
	{
		ptr[i] = malloc(sizeof(char) * 16);
		strcpy(ptr[i], "abcdefghijklmno");
		printf("%s\n", ptr[i]);
	}
	
	for(int i = 9; i >= 0; i--)
	{
		free(ptr[i]);
	}
	free(ptr);
}

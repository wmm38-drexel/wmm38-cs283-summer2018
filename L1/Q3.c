#include <stdlib.h>
#include <stdio.h>

//Swaps two elements of a list
void swap(int* x, int* y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

//Simple bubble sort (not fully optimized)
void sort(int* a, int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (*(a + j) > *(a + j + 1))
				swap((a+j),(a+j+1));
		}
	}

}

//Makes a list (int*) of integers descending 9 to 0, sorts it in ascending
//order, then prints the new list
int main()
{

	int* list = malloc(10 * sizeof(int));

	for(int i = 0; i < 10; i++)
	{
		*(list + i) = 9 - i;
	}

	sort(list, 10);

	for (int i = 0; i < 10; i++)
	{
		printf("%x\n", *(list + i));
	}

	free(list);

}

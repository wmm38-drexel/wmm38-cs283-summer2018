#include <stdlib.h>
#include <stdio.h>

struct array
{
	int size;
	int* data;
};

void addTimesTwo(struct array* a, int n, int data)
{
	while(n > (*a).size - 1)
	{
		(*a).data = realloc((*a).data, ((*a).size*sizeof(int))*2);
		(*a).size*=2;
	}
	(*a).data[n] = data;
}

void addOne(struct array* a, int n, int data)
{
	while(n > (*a).size - 1)
	{
		(*a).data = realloc((*a).data, ((*a).size*sizeof(int)) + sizeof(int));
		(*a).size++;
	}
	(*a).data[n] = data;
}

void main()
{
	struct array a;
	a.data = malloc(sizeof(int));
	a.size = 1;

	int i = 0;
	while(a.size <= 131072)
	{
		addOne(&a, i, i);
		i++;
	}

	for(int j = 0; j < a.size; j++)
	{
		printf("%d\n", a.data[j]);
	}

	free(a.data);
}

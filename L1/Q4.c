#include <stdlib.h>
#include <stdio.h>

struct ListNode
{
	int data;
	struct ListNode* next;
};

void swap(struct ListNode* x, struct ListNode* y)
{
	struct ListNode temp = *x;
	*x = *y;
	*y = temp;

	(*y).next = (*x).next;
	(*x).next = temp.next;
}

void sort(struct ListNode** list)
{
	struct ListNode* i = *list;
	while (i->next)
	{
		int x = i->data;
		struct ListNode* j = i;
		while(j->next)
		{
			if (j->data > j->next->data)
				swap(j, j->next);
			j = j->next;
		}	
		if (x == i->data)
			i = i->next;
	}
}

//Makes a list (ListNode*) of integers descending 4 to 1, sorts it in ascending
//order, then prints the new list
int main()
{
	struct ListNode* n1 = malloc(sizeof(struct ListNode));
	struct ListNode* n2 = malloc(sizeof(struct ListNode));
	struct ListNode* n3 = malloc(sizeof(struct ListNode));
	struct ListNode* n4 = malloc(sizeof(struct ListNode));
	struct ListNode** list = &n4;
	
	n4->data = 4;
	n4->next = n3;
	
	n3->data = 3;
	n3->next = n2;
	
	n2->data = 2;
	n2->next = n1;

	n1->data = 1;
	n1->next = NULL;
	
	sort(list);

	struct ListNode* node = *list;
	while (node)
	{
		printf("%x\n", node->data);
		node = node->next;
	}
  
	free(n1);
	free(n2);
	free(n3);
	free(n4);
}

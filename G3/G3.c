#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

const char slotChars[3] = {'X', 'P', 'C'};
static int	pfd1[2], pfd2[2];
int board[BOARDSIZE][BOARDSIZE];

static int sockets[2];

void printBoard()
{
    printf("\n");
    for (int i = 0; i < BOARDSIZE; i++)
    {
        for (int j = 0; j < BOARDSIZE; j++)
        {
            printf("%c ", slotChars[board[i][j]]);
        }
        printf("\n");
    }
    printf("\n");
}

int isWinner(int player, int row, int col)
{
    int horizCheck = 0;
    for (int i = -3; i <= 3; i++)
    {
        if (col + i >= 0 && col + i < BOARDSIZE && board[row][col + i] == player)
            horizCheck++;
        else
            horizCheck = 0;
        if (horizCheck >= 4)
            return 1;
    }

    int vertCheck = 0;
    for (int i = -3; i <= 3; i++)
    {
        if (row + i >= 0 && row + i < BOARDSIZE && board[row + i][col] == player)
            vertCheck++;
        else
            vertCheck = 0;
        if (vertCheck >= 4)
            return 1;
    }

    int backDiag = 0;
    for (int i = -3; i <= 3; i++)
    {
        if (    row + i >= 0 &&
                col + i >= 0 &&
                row + i <= BOARDSIZE &&
                col + i <= BOARDSIZE &&
                board[row + i][col + i] == player   )
            backDiag++;
        else
            backDiag = 0;
        if (backDiag >= 4)
            return 1;
    }

    int frontDiag = 0;
    for (int i = -3; i <= 3; i++)
    {
        if (    row + i >= 0 &&
                col - i >= 0 &&
                row + i <= BOARDSIZE &&
                col - i <= BOARDSIZE &&
                board[row + i][col - i] == player   )
            frontDiag++;
        else
            frontDiag = 0;
        if (frontDiag >= 4)
            return 1;
    }

    return 0;
}

void TELL_WAIT()
{
	if (pipe(pfd1) < 0 || pipe(pfd2) < 0)
		{ printf("pipe error\n"); exit(1); }
}

void TELL_PARENT(pid_t pid)
{
	if (write(pfd2[1], "c", 1) != 1)
		{ printf("write error"); exit(1); }
}

void WAIT_PARENT(void)
{
	char	c;

	if (read(pfd1[0], &c, 1) != 1)
		{ printf("read error"); exit(1); }
	if (c != 'p')
		{ printf("WAIT_PARENT: incorrect data\n"); exit(1); }
}

void TELL_CHILD(pid_t pid)
{
	if (write(pfd1[1], "p", 1) != 1)
		{ printf("write error"); exit(1); }
}

void WAIT_CHILD(void)
{
	char	c;

	if (read(pfd2[0], &c, 1) != 1)
		{ printf("read error\n"); exit(1); }
	if (c != 'c')
		{ printf("WAIT_CHILD: incorrect data\n"); exit(1); }
}

void main()
{
    TELL_WAIT();
    srand(time(NULL));

    pid_t pid;
    /* Create a pipe */
    if (pipe(sockets) < 0)
    {
        perror("Opening stream socket pair");
        exit(10);
    }
    if ((pid = fork()) == -1)
        perror("Forking");  
    else if (pid != 0)
    {
        int game = 1;
        while (game) 
        {
            // Wait for go-ahead from child
            WAIT_CHILD();

            // Get and print board
            if (read(sockets[0], board, sizeof(int) * BOARDSIZE * BOARDSIZE) < 0)
                perror("Parent read error.");
            printBoard();
            printf("%s\n", "Parent's turn:");

            // Parent takes turn
            int column;
            printf("What column would you like to play to?\n");
            scanf("%d", &column);
            int index = 0;
            while (board[index][column - 1] == 0 && index < BOARDSIZE) index++;
            board[index - 1][column - 1] = 1;

            if (isWinner(1, index - 1, column - 1))
            {
                printBoard();
                printf("Parent won!\n");
                game = 0;
                break;
            }
            if (write(sockets[1], board, sizeof(int) * BOARDSIZE * BOARDSIZE) < 0)
                perror("Parent write error.");

            TELL_CHILD(pid);
        }

        close(sockets[0]);
        close(sockets[1]);
    }
    else
    {
        int game = 1;
        while (game)
        {
            // Change the board
            int column = rand() % 5;
            int index = 0;
            while (board[index][column] == 0 && index < BOARDSIZE) index++;
            board[index - 1][column] = 2;

            if (isWinner(2, index - 1, column))
            {
                printBoard();
                printf("Child won!\n");
                close(sockets[0]);
                close(sockets[1]);
                kill(pid, SIGKILL);
                break;
            }

            // Write board and tell parent it's ready
            if (write(sockets[1], board, sizeof(int) * BOARDSIZE * BOARDSIZE) < 0)
                perror("Child write error.");
            TELL_PARENT(pid);

            WAIT_PARENT();
            if (read(sockets[0], board, sizeof(int) * BOARDSIZE * BOARDSIZE) < 0)
                perror("Child read error.");
        }

        close(sockets[0]);
        close(sockets[1]);
    }
}